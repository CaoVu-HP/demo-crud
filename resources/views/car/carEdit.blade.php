@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                   <h4>Edit Car</h4>
                </div>
                   
                <div class="modal-body">
                <form method="post" action="{{route('updateCar')}}" >
                @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" name="name" class="form-control" value="{{$singleCar->name}}" placeholder="Enter Name" />
                        <input type="hidden" name="id"   class="form-control" value="{{$singleCar->id}}" placeholder="Enter Name" />
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Brand</label>
                        <input type="text" name="brand" class="form-control" value="{{$singleCar->brand}}" placeholder="Enter Brand" />
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Price</label>
                        <input type="text" name="price" class="form-control" value="{{$singleCar->price}}" placeholder="Enter Price" />
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Speed</label>
                        <input type="text" name="speed" class="form-control" value="{{$singleCar->speed}}" placeholder="Enter Speed" />
                    </div>
                    <div class="modal-footer">
                    <button type="button" onclick="window.location='{{ route("car") }}'" class="btn btn-secondary" >Cancel</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection