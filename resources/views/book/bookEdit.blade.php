@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                   <h4>Edit Book</h4>
                </div>
                   
                <div class="modal-body">
                <form method="post" action="{{route('updateBook')}}" >
                @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" name="name" class="form-control" value="{{$singleBook->name}}" placeholder="Enter Name" />
                        <input type="hidden" name="id"   class="form-control" value="{{$singleBook->id}}" placeholder="Enter Name" />
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Type</label>
                        <input type="text" name="type" class="form-control" value="{{$singleBook->type}}" placeholder="Enter Type" />
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Price</label>
                        <input type="text" name="price" class="form-control" value="{{$singleBook->price}}" placeholder="Enter Price" />
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Pages</label>
                        <input type="number" name="pages" class="form-control" value="{{$singleBook->pages}}" placeholder="Enter Pages" />
                    </div>
                    <div class="modal-footer">
                    <button type="button" onclick="window.location='{{ route("book") }}'" class="btn btn-secondary" >Cancel</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection