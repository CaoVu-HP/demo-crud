@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#exampleModal">Create New</button>
                </div>
                    <p class="text-success" style="text-align: center">{{Session::get('message')}}</p>
                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                      
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Dogs Breed</th>
                                <th scope="col">Gender</th>
                                <th scope="col">Age</th>
                                <th scope="col">Action</th>
                            </tr>
                          
                        </thead>
                        <tbody>
                        @php($i=1)
                        @foreach($allPet as $pet)
                            <tr>
                                <th scope="row">{{$i++}}</th>
                                <td>{{$pet->name}}</td>
                                <td>{{$pet->DogsBreed}}</td>
                                <td>{{$pet->gender}}</td>
                                <td>{{$pet->age}}</td>
                                <td>
                                    <a href="{{route('editPet',['id'=>$pet->id])}}">Edit</a>
                                    <a href="{{route('deletePet',['id'=>$pet->id])}}"
                                    onclick="return confirm('Are you sure to delete this')">Delete</a>
                                </td>
                            </tr>
                            @endforeach  
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create new Pet</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="{{route('savePet')}}" >
                @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" name="name" class="form-control" placeholder="Enter Name" />
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">DogsBreed</label>
                        <input type="text" name="DogsBreed" class="form-control" placeholder="Enter DogsBreed" />
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Gender</label>
                        <input type="text" name="gender" class="form-control" placeholder="Enter Gender" />
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Age</label>
                        <input type="number" name="age" class="form-control" placeholder="Enter Age" />
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection