@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                   <h4>Edit Pet</h4>
                </div>
                   
                <div class="modal-body">
                <form method="post" action="{{route('updatePet')}}" >
                @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" name="name" class="form-control" value="{{$singlePet->name}}" placeholder="Enter Name" />
                        <input type="hidden" name="id"   class="form-control" value="{{$singlePet->id}}" placeholder="Enter Name" />
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">DogsBreed</label>
                        <input type="text" name="DogsBreed" class="form-control" value="{{$singlePet->DogsBreed}}" placeholder="Enter Gender" />
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Gender</label>
                        <input type="text" name="gender" class="form-control" value="{{$singlePet->gender}}" placeholder="Enter Age" />
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Age</label>
                        <input type="number" name="age" class="form-control" value="{{$singlePet->age}}" placeholder="Enter Age" />
                    </div>
                    <div class="modal-footer">
                    <button type="button" onclick="window.location='{{ route("pet") }}'" class="btn btn-secondary" >Cancel</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection