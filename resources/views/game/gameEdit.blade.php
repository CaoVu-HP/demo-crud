@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                   <h4>Edit Game</h4>
                </div>
                   
                <div class="modal-body">
                <form method="post" action="{{route('updateGame')}}" >
                @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" name="name" class="form-control" value="{{$singleGame->name}}" placeholder="Enter Name" />
                        <input type="hidden" name="id"   class="form-control" value="{{$singleGame->id}}" placeholder="Enter Name" />
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Type</label>
                        <input type="text" name="type" class="form-control" value="{{$singleGame->type}}" placeholder="Enter Type" />
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Players Per Day</label>
                        <input type="number" name="players" class="form-control" value="{{$singleGame->players}}" placeholder="Enter Number" />
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Price</label>
                        <input type="text" name="price" class="form-control" value="{{$singleGame->price}}" placeholder="Enter Price" />
                    </div>

                   
                    <div class="modal-footer">
                    <button type="button" onclick="window.location='{{ route("game") }}'" class="btn btn-secondary" >Cancel</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection