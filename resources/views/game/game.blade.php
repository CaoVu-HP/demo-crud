@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#exampleModal">Create New</button>
                </div>
                    <p class="text-success" style="text-align: center">{{Session::get('message')}}</p>
                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Type</th>
                                <th scope="col">Players Per Day</th>
                                <th scope="col">Price</th>
                                <th scope="col">Action</th>
                            </tr>
                          
                        </thead>
                        <tbody>
                        @php($i=1)
                        @foreach($allGame as $game)
                            <tr>
                                <th scope="row">{{$i++}}</th>
                                <td>{{$game->name}}</td>
                                <td>{{$game->type}}</td>
                                <td>{{$game->players}}</td>
                                <td>{{$game->price}}</td>
                                <td>
                                    <a href="{{route('editGame',['id'=>$game->id])}}">Edit</a>
                                    <a href="{{route('deleteGame',['id'=>$game->id])}}"
                                    onclick="return confirm('Are you sure to delete this')">Delete</a>
                                </td>
                            </tr>
                            @endforeach  
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create new Game</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="{{route('saveGame')}}" >
                @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" name="name" class="form-control" placeholder="Enter Name" />
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Type</label>
                        <input type="text" name="type" class="form-control" placeholder="Enter Type" />
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Players Per Day</label>
                        <input type="number" name="players" class="form-control" placeholder="Enter Number" />
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Price</label>
                        <input type="text" name="price" class="form-control" placeholder="Enter Price" />
                    </div>
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection