@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                   <h4>Edit ItemPerson</h4>
                </div>
                   
                <div class="modal-body">
                <form method="post" action="{{route('updateItem')}}" >
                @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" name="name" class="form-control" value="{{$singleItem->name}}" placeholder="Enter Name" />
                        <input type="hidden" name="id"   class="form-control" value="{{$singleItem->id}}" placeholder="Enter Name" />
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Gender</label>
                        <input type="text" name="gender" class="form-control" value="{{$singleItem->gender}}" placeholder="Enter Gender" />
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Age</label>
                        <input type="number" name="age" class="form-control" value="{{$singleItem->age}}" placeholder="Enter Age" />
                    </div>
                    <div class="modal-footer">
                    <button type="button" onclick="window.location='{{ route("item") }}'" class="btn btn-secondary" >Cancel</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection