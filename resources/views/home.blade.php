@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-success" style="text-align: center;">{{('Login success!')}}</div>
                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <div style="text-align: center;">
                        <a class="navbar-brand" style="margin-left" href="{{ route('item') }}">
                            {{ 'PerSon' }}
                        </a>
                        <a class="navbar-brand" href="{{ route('book') }}">
                            {{ 'Book' }}
                        </a>

                        <a class="navbar-brand" href="{{ route('pet') }}">
                            {{ 'Pet' }}
                        </a>

                        <a class="navbar-brand" href="{{ route('car') }}">
                            {{ 'Car' }}
                        </a>
                        <a class="navbar-brand" href="{{ route('game') }}">
                            {{ 'Game' }}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection