<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Book;
use Session;
use App\Services\BookService;
class BookController extends Controller
{
    //
    private $bookService;
    /**
     * GroupController constructor.
     * @param $groupService
     */
    public function __construct(BookService $bookService)
    {
        $this->bookService = $bookService;
    }
  
    public function index()
    {
        // $books=Book::orderBy('created_at')->get();
        // return view('book.book',['allBook'=>$books]);
        $books=$this->bookService->getAllBook();
        return view('book.book',['allBook'=>$books]);
    }
    public function saveBook(Request $request)
    {
        $this->bookService->insertBook($request);
        return redirect('/book')->with('message','Book has been created successfully');
    }
    public function edit($id)
    {
        $book=$this->bookService->findBook($id);
        return view('book.bookEdit',['singleBook'=>$book]);
    }
    public function update( Request $request)
    {
        //  $book=Book::find($request->id);
        //  $book ->name =$request ->name;
        //  $book ->type =$request ->type;
        //  $book ->players =$request ->players;
        //  $book ->price =$request ->price;
        //  $book ->save();
         $this->bookService->saveBook($request);
        return redirect('/book')->with('message','Book has been Updated successfully');
    }
    public function delete($id)
    {
        $book=$this->bookService->delete($id);
        return redirect('/book')->with('message','Book has been Deleted successfully');
    }

}
