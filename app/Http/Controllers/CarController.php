<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Car;
use Session;
use App\Services\CarService;
class CarController extends Controller
{
    //
    private $carService;
    /**
     * GroupController constructor.
     * @param $groupService
     */
    public function __construct(CarService $carService)
    {
        $this->carService = $carService;
    }
  
    public function index()
    {
        // $cars=Car::orderBy('created_at')->get();
        // return view('car.car',['allCar'=>$cars]);
        $cars=$this->carService->getAllCar();
        return view('car.car',['allCar'=>$cars]);
    }
    public function CarSave(Request $request)
    {
        $this->carService->insertCar($request);
        return redirect('/car')->with('message','Car has been created successfully');
    }
    public function Edit($id)
    {
        $car=$this->carService->findCar($id);
        return view('car.carEdit',['singleCar'=>$car]);
    }
    public function Update( Request $request)
    {
        //  $car=Car::find($request->id);
        //  $car ->name =$request ->name;
        //  $car ->type =$request ->type;
        //  $car ->players =$request ->players;
        //  $car ->price =$request ->price;
        //  $car ->save();
         $this->carService->saveCar($request);
        return redirect('/car')->with('message','Car has been Updated successfully');
    }
    public function Delete($id)
    {
        $car=$this->carService->Delete($id);
        return redirect('/car')->with('message','Car has been Deleted successfully');
    }

}
