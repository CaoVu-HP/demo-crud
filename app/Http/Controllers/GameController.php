<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Game;
use Session;
use App\Services\GameService;
class GameController extends Controller
{
    //
    private $gameService;
    /**
     * GroupController constructor.
     * @param $groupService
     */
    public function __construct(GameService $gameService)
    {
        $this->gameService = $gameService;
    }
  
    public function index()
    {
        // $games=Game::orderBy('created_at')->get();
        // return view('game.game',['allGame'=>$games]);
        $games=$this->gameService->getAllGame();
        return view('game.game',['allGame'=>$games]);
    }
    public function GameSave(Request $request)
    {
        $this->gameService->insertGame($request);
        return redirect('/game')->with('message','Game has been created successfully');
    }
    public function Edit($id)
    {
        $game=$this->gameService->findGame($id);
        return view('game.gameEdit',['singleGame'=>$game]);
    }
    public function Update( Request $request)
    {
        //  $game=Game::find($request->id);
        //  $game ->name =$request ->name;
        //  $game ->type =$request ->type;
        //  $game ->players =$request ->players;
        //  $game ->price =$request ->price;
        //  $game ->save();
         $this->gameService->saveGame($request);
        return redirect('/game')->with('message','Game has been Updated successfully');
    }
    public function Delete($id)
    {
        $game=$this->gameService->Delete($id);
        return redirect('/game')->with('message','Game has been Deleted successfully');
    }

}
