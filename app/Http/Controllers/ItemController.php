<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Item;
use Session;
use App\Services\ItemService;
class ItemController extends Controller
{
    //
    private $itemService;
    /**
     * GroupController constructor.
     * @param $groupService
     */
    public function __construct(ItemService $itemService)
    {
        $this->itemService = $itemService;
    }
  
    public function index()
    {
        // $items=Item::orderBy('created_at')->get();
        // return view('item.item',['allItem'=>$items]);
        $items=$this->itemService->getAllItem();
        return view('item.item',['allItem'=>$items]);
    }
    public function ItemSave(Request $request)
    {
        $this->itemService->insertItem($request);
        return redirect('/item')->with('message','Item has been created successfully');
    }
    public function Edit($id)
    {
        $item=$this->itemService->findItem($id);
        return view('item.itemEdit',['singleItem'=>$item]);
    }
    public function Update( Request $request)
    {
        //  $item=Item::find($request->id);
        //  $item ->name =$request ->name;
        //  $item ->type =$request ->type;
        //  $item ->players =$request ->players;
        //  $item ->price =$request ->price;
        //  $item ->save();
         $this->itemService->saveItem($request);
        return redirect('/item')->with('message','Item has been Updated successfully');
    }
    public function Delete($id)
    {
        $item=$this->itemService->Delete($id);
        return redirect('/item')->with('message','Item has been Deleted successfully');
    }

}
