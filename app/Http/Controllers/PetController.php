<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Pet;
use Session;
use App\Services\PetService;
class PetController extends Controller
{
    //
    private $petService;
    /**
     * GroupController constructor.
     * @param $groupService
     */
    public function __construct(PetService $petService)
    {
        $this->petService = $petService;
    }
  
    public function index()
    {
        // $pets=Pet::orderBy('created_at')->get();
        // return view('pet.pet',['allPet'=>$pets]);
        $pets=$this->petService->getAllPet();
        return view('pet.pet',['allPet'=>$pets]);
    }

    public function PetSave(Request $request)
    {
        $this->petService->insertPet($request);
        return redirect('/pet')->with('message','Pet has been created successfully');
    }

    public function Edit($id)
    {
        $pet=$this->petService->findPet($id);
        return view('pet.petEdit',['singlePet'=>$pet]);
    }

    public function Update( Request $request)
    {
        //  $pet=Pet::find($request->id);
        //  $pet ->name =$request ->name;
        //  $pet ->type =$request ->type;
        //  $pet ->players =$request ->players;
        //  $pet ->price =$request ->price;
        //  $pet ->save();
         $this->petService->savePet($request);
        return redirect('/pet')->with('message','Pet has been Updated successfully');
    }

    public function Delete($id)
    {
        $pet=$this->petService->Delete($id);
        return redirect('/pet')->with('message','Pet has been Deleted successfully');
    }
}
