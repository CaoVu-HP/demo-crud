<?php


namespace App\Services;
use App\Repositories\PetRepository;

class PetService
{
    private $petRepository;

    /**
     * GroupService constructor.
     * @param $groupRepository
     */
    public function __construct(PetRepository $petRepository)
    {
        $this->petRepository = $petRepository;
    }

    public function getAllPet()
    {
        return $this->petRepository->getAllPet();
       
    }

    public function insertPet($input)
    {
        return  $this->petRepository->insertPet($input);
    }

    public function findPet($id)
    {
        return  $this->petRepository->findPet($id);
    }

    public function Edit($id)
    {
        $pet= $this->petRepository->findPet($id);
    }

    public function savePet( $input)
    {
        return $this->petRepository->savePet($input);
    }

    public function Delete($id)
    {
        $pet= $this->petRepository->findPet($id);
        return $pet->delete();
    }
}
