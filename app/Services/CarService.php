<?php


namespace App\Services;
use App\Repositories\CarRepository;

class CarService
{
    private $carRepository;

    /**
     * GroupService constructor.
     * @param $groupRepository
     */
    public function __construct(CarRepository $carRepository)
    {
        $this->carRepository = $carRepository;
    }

    public function getAllCar()
    {
        return $this->carRepository->getAllCar();
       
    }

    public function insertCar($input)
    {
        return  $this->carRepository->insertCar($input);
    }

    public function findCar($id)
    {
        return  $this->carRepository->findCar($id);
    }

    public function Edit($id)
    {
        $car= $this->carRepository->findCar($id);
    }

    public function saveCar( $input)
    {
        return $this->carRepository->saveCar($input);
    }

    public function Delete($id)
    {
        $car= $this->carRepository->findCar($id);
        return $car->delete();
    }
}
