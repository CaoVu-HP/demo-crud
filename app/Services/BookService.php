<?php


namespace App\Services;
use App\Models\Book;
use App\Repositories\BookRepository;

class BookService
{
    private $bookRepository;

    /**
     * GroupService constructor.
     * @param $groupRepository
     */
    public function __construct(BookRepository $bookRepository)
    {
        $this->bookRepository = $bookRepository;
    }

    public function getAllBook()
    {
        return $this->bookRepository->getAllBook();
       
    }

    public function insertBook($param)
    {
        $validator = $param->validate([
            'name' => 'required',
            'type' => 'required',
            'price' => 'required',
            'pages' => 'required'
                                    ]);  
            $book = new Book();
            $book ->name  =$param ->name;
            $book ->type  =$param ->type;
            $book ->price =$param ->price;
            $book ->pages =$param ->pages;  
        return  $this->bookRepository->insertBook($book);
    }

    public function findBook($id)
    {
        return  $this->bookRepository->findBook($id);
    }

    public function edit($id)
    {
        $book= $this->bookRepository->findBook($id);
    }

    public function saveBook( $param)
    {
        $book=Book::find($param->id);
        $book ->name  =$param ->name;
        $book ->type  =$param ->type;
        $book ->price =$param ->price;
        $book ->pages =$param ->pages;
        return  $this->bookRepository->insertBook($book);
    }

    public function delete($param)
    {
        $this->bookRepository->delete($param);
    }
}
