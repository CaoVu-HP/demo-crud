<?php


namespace App\Services;
use App\Repositories\GameRepository;

class GameService
{
    private $gameRepository;

    /**
     * GroupService constructor.
     * @param $groupRepository
     */
    public function __construct(GameRepository $gameRepository)
    {
        $this->gameRepository = $gameRepository;
    }

    public function getAllGame()
    {
        return $this->gameRepository->getAllGame();
       
    }

    public function insertGame($input)
    {
        return  $this->gameRepository->insertGame($input);
    }

    public function findGame($id)
    {
        return  $this->gameRepository->findGame($id);
    }

    public function Edit($id)
    {
        $game= $this->gameRepository->findGame($id);
    }

    public function saveGame( $input)
    {
        return $this->gameRepository->saveGame($input);
    }

    public function Delete($id)
    {
        $game= $this->gameRepository->findGame($id);
        return $game->delete();
    }
}
