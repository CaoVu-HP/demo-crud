<?php


namespace App\Services;
use App\Repositories\ItemRepository;

class ItemService
{
    private $itemRepository;

    /**
     * GroupService constructor.
     * @param $groupRepository
     */
    public function __construct(ItemRepository $itemRepository)
    {
        $this->itemRepository = $itemRepository;
    }

    public function getAllItem()
    {
        return $this->itemRepository->getAllItem();
       
    }

    public function insertItem($input)
    {
        return  $this->itemRepository->insertItem($input);
    }

    public function findItem($id)
    {
        return  $this->itemRepository->findItem($id);
    }

    public function Edit($id)
    {
        $item= $this->itemRepository->findItem($id);
    }

    public function saveItem( $input)
    {
        return $this->itemRepository->saveItem($input);
    }

    public function Delete($id)
    {
        $item= $this->itemRepository->findItem($id);
        return $item->delete();
    }
}
