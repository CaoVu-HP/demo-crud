<?php
namespace App\Repositories;
use App\Models\Book;
class BookRepository
{
    // Get all Book  
    public function getAllBook(){
        return Book::orderBy('created_at')->get();
    }
    
    public function insertBook( $book)
    { 
         return $book ->save();
    }

    public function findBook($book)
    {
         return  Book::find($book);
    }

    public function delete($book)
    {
        Book::where('_id', $book)->first()->delete();
    }
}

