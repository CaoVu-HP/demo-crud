<?php
namespace App\Repositories;
use Session;
use App\Models\Pet;
class PetRepository
{
    public function getAllPet(){
        return Pet::orderBy('created_at')->get();
    }
    
    public function insertPet( $input)
    {
        $validated = $input->validate([
        'name'      => 'required',
        'DogsBreed' => 'required',
        'gender'    => 'required',
        'age'       => 'required'
                                ]);
         $pet = new Pet();
         $pet ->name       =$input ->name;
         $pet ->DogsBreed  =$input ->DogsBreed;
         $pet ->gender     =$input ->gender;
         $pet ->age        =$input ->age;
         return  $pet ->save();
    }

    public function findPet($id)
    {
         return  Pet::find($id);
    }

    public function savePet($input)
    {
         $pet=Pet::find($input->id);
         $pet ->name        =$input ->name;
         $pet ->DogsBreed   =$input ->DogsBreed;
         $pet ->gender      =$input ->gender;
         $pet ->age         =$input ->age;
         $pet ->save();
    }
}

