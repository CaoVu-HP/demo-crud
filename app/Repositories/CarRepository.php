<?php
namespace App\Repositories;
use Session;
use App\Models\Car;
class CarRepository
{
    public function getAllCar(){
        return Car::orderBy('created_at')->get();
    }
    public function insertCar( $input)
    {
        $validated = $input->validate([
        'name' => 'required',
        'brand' => 'required',
        'price' => 'required',
        'speed' => 'required'
                                ]);
         $car = new Car();
         $car ->name  =$input ->name;
         $car ->brand  =$input ->brand;
         $car ->price =$input ->price;
         $car ->speed =$input ->speed;
         return  $car ->save();
    }

    public function findCar($id)
    {
         return  Car::find($id);
    }
    public function saveCar($input)
    {
         $car=Car::find($input->id);
         $car ->name  =$input ->name;
         $car ->brand  =$input ->brand;
         $car ->price =$input ->price;
         $car ->speed =$input ->speed;
         $car ->save();
    }
}

