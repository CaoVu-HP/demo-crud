<?php
namespace App\Repositories;
use Session;
use App\Models\Game;
class GameRepository
{
    public function getAllGame(){
        return Game::orderBy('created_at')->get();
    }
    public function insertGame( $input)
    {
        $validated = $input->validate([
        'name' => 'required',
        'type' => 'required',
        'players' => 'required',
        'price' => 'required'
                                ]);
         $game = new Game();
         $game ->name  =$input ->name;
         $game ->type  =$input ->type;
         $game ->players =$input ->players;
         $game ->price =$input ->price;
         return  $game ->save();
    }

    public function findGame($id)
    {
         return  Game::find($id);
    }
    public function saveGame($input)
    {
         $game=Game::find($input->id);
         $game ->name  =$input ->name;
         $game ->type  =$input ->type;
         $game ->players =$input ->players;
         $game ->price =$input ->price;
         $game ->save();
    }
}

