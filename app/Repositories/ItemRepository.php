<?php
namespace App\Repositories;
use Session;
use App\Models\Item;
class ItemRepository
{
    public function getAllItem(){
        return Item::orderBy('created_at')->get();
    }
    public function insertItem( $input)
    {
        $validated = $input->validate([
        'name' =>   'required',
        'gender' => 'required',
        'age' =>    'required',
                                ]);
         $item = new Item();
         $item ->name   =$input ->name;
         $item ->gender =$input ->gender;
         $item ->age    =$input ->age;
         return  $item ->save();
    }

    public function findItem($id)
    {
         return Item::find($id);
    }
    public function saveItem($input)
    {
         $item= Item::find($input->id);
         $item ->name  =$input ->name;
         $item ->gender  =$input ->gender;
         $item ->age =$input ->age;
         $item ->save();
    }
}

