<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\PetController;
use App\Http\Controllers\CarController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\GameController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home',[HomeController::class, 'index'])->name('home');
Route::get('/item', [ItemController::class, 'index'])->name('item');
Route::post('/item/saveItem',[ItemController::class, 'ItemSave'])->name('saveItem');
Route::get('/editItem/{id}',[ItemController::class, 'Edit'])->name('editItem');
Route::post('updateItem',[ItemController::class, 'Update'])->name('updateItem');
Route::get('/deleteItem/{id}',[ItemController::class, 'Delete'])->name('deleteItem');

Route::get('/pet', [PetController::class, 'index'])->name('pet');
Route::post('/pet/savePet',[PetController::class, 'PetSave'])->name('savePet');
Route::get('/editPet/{id}',[PetController::class, 'Edit'])->name('editPet');
Route::post('updatePet',[PetController::class, 'Update'])->name('updatePet');
Route::get('/deletePet/{id}',[PetController::class, 'Delete'])->name('deletePet');

Route::get('/car', [CarController::class, 'index'])->name('car');
Route::post('/car/saveCar',[CarController::class, 'CarSave'])->name('saveCar');
Route::get('/editCar/{id}',[CarController::class, 'Edit'])->name('editCar');
Route::post('updateCar',[CarController::class, 'Update'])->name('updateCar');
Route::get('/deleteCar/{id}',[CarController::class, 'Delete'])->name('deleteCar');

Route::get('/book', [BookController::class, 'index'])->name('book');
Route::post('/book/saveBook',[BookController::class, 'saveBook'])->name('saveBook');
Route::get('/editBook/{id}',[BookController::class, 'edit'])->name('editBook');
Route::post('updateBook',[BookController::class, 'update'])->name('updateBook');
Route::get('/deleteBook/{id}',[BookController::class, 'delete'])->name('deleteBook');

Route::get('/game', [GameController::class, 'index'])->name('game');
Route::post('/game/saveGame',[GameController::class, 'GameSave'])->name('saveGame');
Route::get('/editGame/{id}',[GameController::class, 'Edit'])->name('editGame');
Route::post('updateGame',[GameController::class, 'Update'])->name('updateGame');
Route::get('/deleteGame/{id}',[GameController::class, 'Delete'])->name('deleteGame');
